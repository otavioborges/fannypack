# Fannypack - Data package generator

The Fannypack project goal is to deliver a multi-platform C library to create data packages to be securetly transmit between stations. It is build to be lightweight so the recipient can be an embedded application, therefore allowing secure firmware updates.

## Package Data Structure

A data package is split in two chunks: **HEADER** and **DATA**. **DATA** is basically the encrypted compressed data chunk. **HEADER** can have two formats: *normal* and *strap*, *strap* headers are smaller, tottaling **X** bytes, *normal* header length is **X** bytes. The data structure of both is as follows:

| Name                       | Header Format | Length(in bytes) | Description                                                                                                |
|----------------------------|---------------|------------------|------------------------------------------------------------------------------------------------------------|
| **MAGIC**					 | Both          | 4                | It's magical! Fannypack package identifier                                                                 |
| **HEADER_TYPE**			 | Both          | 1                | Indicates the format of the header, 0x00 for normal, 0x01 for strap, 0x02-0xFF is reserved                 |
| **LIB_VERSION**			 | Both          | 4                | The version of the fannypack library that created the package in the format MM.mm.Rev.Arch                 |
| **PACKAGE_UID**            | Both          | 4                | Somewhat unique identifier for this package, can be randomly generated                                     |
| **USER_DEFINED1**			 | Both          | 8                | User data, duh!                                                                                            |
| **DATA_CHUNK_CRC**		 | Both          | 2                | CRC for the compressed data chunk                                                                          |
| **DATA_TRANSMIT_LENGTH**	 | Both          | 4                | Encryption might require trailing zeros, the encrypted data might be slightly larger than compressed data  |
| **DATA_COMPRESSED_LENGTH** | Both          | 4                | The length of the compressed data chunk                                                                    |
| **DATA_FULL_LENGTH**		 | Both          | 4                | Data length when uncompressed                                                                              |
| **VERSION**				 | Both          | 4                | For update control, version of the application on the data chunk, on the format MM.mm.Rev.User             |
| **CRYPT_METHOD**			 | Both          | 1                | Encryption method used, use section *Encryption Methods* for possible methods                              |
| **CRYPT_IV**				 | Both          | 1                | Initialization vector for AES cypher                                                                       |
| **TIMESTAMP**				 | Normal        | 4                | Unix Timestamp of the package creation date                                                                |
| **DESCRIPTION**			 | Normal        | 96               | String with the description of the package                                                                 |
| **USER_DEFINED2**			 | Normal        | 32               | Application-dependent bytes, extended from USER_DEFINED1                                                   |
| **RESERVED				 | Normal        | 92               | Reserved for future use, all zeores                                                                        |

## Error responses

| Name                     | Value | Description                                                                   |
|--------------------------|-------|-------------------------------------------------------------------------------|
| **SUCCESS**              | 0     | All good, baby                                                                |
| **WRONG_DATA**           | -1    | Bad, bad input data, probably not a Fannypack package                         |
| **ILLEGAL_CRYPT_METHOD** | -2    | The selected cryptography method is not valid                                 |
| **NOT_IMPLEMENTED**      | -3    | Function or method not available in this version                              |
| **NOT_A_FANNYPACK_BRO**  | -4    | Definetly not a Fannypack Package                                             |
| **THREAD_ERROR**         | -5    | Error creating internal process threads                                       |
| **INIT_ZLIB**            | -6    | Zlib internal functions returned an error                                     |
| **COMPRESSING**          | -7    | Error while compressing/umcompressing the payload                             |
| **INVALID_PATH**         | -8    | Provided path is not valid (Hierarchy invalid)                                |
| **EACCES**               | -9    | Error creating files or folder on destination                                 |
| **FILE_CREATION**        | -10   | Error creating file                                                           |
| **FILE_READING**         | -11   | Error reading input data from disk                                            |
| **FILE_WRITING**         | -12   | Error writting file to disk                                                   |
| **FILE_SIZE_MISMATCH**   | -13   | Mismatch between payload informed file size and actual written data           |
| **CHUNK_TOO_SMALL**      | -14   | For chunked operations, the provided size is not big enough for the operation |

## Chunked Implementation

Within the *Chunked* methods the necessary memory to execute methods is reduced. As chunks are processed they are written back to disk.

This library is intended to be used on a *x86/AMD64* architecture for packages generation and those will be read and unpacked on a embedded application. Therefore, only
unpacking related methods (*eg.* *FANNYPACK_ChunksWriteArchiverEntries* and *FANNYPACK_ChunksExtractData*) have chunked implementations.

## Archiver

The archiver is the library routines to handle files and folder as the payload. The methods *FANNYPACK_CreateDataArchiver*, *FANNYPACK_WriteArchiverEntries*
and *FANNYPACK_ChunksWriteArchiverEntries* transforms and reads (as a stream or chunks of data) the payload as archives and folders. Those methods allow the secure
transmition of compressed folders. 

# Available methods

1. *FANNYPACK_CreateDataArchiver*: Read a given folder and construct the payload based on it. Returns the pointer to payload;
   -*const char \*path* - Path to input folder;
   -*const char \*option* - Not used, leave blank or NULL;
   -*uint32_t \*length* - The resulting package length in bytes.
  
2. *FANNYPACK_WriteArchiverEntries*: Reads the decypted, uncompressed payload and writes down the files. Returns **Error reponse**;
   -*const char \*destination* - The path where data will be written;
   -*uint8_t \*data* - Pointer to input payload.
  
3. *FANNYPACK_ChunksWriteArchiverEntries*: Same as *FANNYPACK_WriteArchiverEntries* but in chunks;
   -*const char \*source*: Location of decypted, uncompressed payload in the disk;
   -*const char \*destination*: Destination where files will be written;
   -*uint32_t maxChunks*: Maximum data chunk per time.
  
4. Other methods are available....will get to then one day.


# Handbonning Capabilities

YES! **MANY**
