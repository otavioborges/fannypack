/*
 * argParser.h
 *
 *  Created on: Dec 19, 2020
 *      Author: otavio
 */

#ifndef INC_ARGPARSER_H_
#define INC_ARGPARSER_H_

struct argument{
	char *option;
	char *value;
	struct argument *next;
};

struct argument *parseTheArguments(int argc, char **argv, int *listLength);

#endif /* INC_ARGPARSER_H_ */
