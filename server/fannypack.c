#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdarg.h>
#include <sys/time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <errno.h>

#include "argParser.h"
#include "libfannypack.h"

#define RESULT_UDP_ERROR_SOCKET         -1
#define RESULT_UDP_ERROR_OUR_IP         -2
#define RESULT_BAD_ARGUMENTS            -3
#define RESULT_BAD_INPUT_FILE           -4
#define RESULT_BAD_OUTPUT_FILE          -5
#define RESULT_STACK_OVERFLOW           -6
#define RESULT_ERROR_CREATING_PACKAGE   -7
#define RESULT_ERROR_THREAD             -8
#define RESULT_BAD_TCP_SERVER           -9

#define TEMP_DATA_FILE                  "/tmp/fannyTemp"
#define DEFAULT_FILE_NAME               "fannypack_data"
#define CHUNK_SIZE                      512

#define DATA_LENGTH		(5*1024)

struct argument *arguments = NULL;
static uint8_t theKey[] = {0x66, 0xB4, 0x7A, 0x27, 0x7B, 0x41, 0x73, 0xFF, 0x71, 0xBD, 0xC6, 0x2D, 0x0B, 0x04, 0xFE, 0x2B};

static int g_serverSocket = -1;
static int g_running = 1;

static void stopServer(int socketNum, pthread_t *thread);
static void *enquiryServer(void *argument);
static void fitDataSize(double value, char *wat);
static void fitTime(double value, char *wat);
static int printHelp(int rtnValue, char *message, ...);

#define SERVER_METHOD_FILE          1
#define SERVER_METHOD_DIR           2
#define SERVER_METHOD_SERVER        3
#define SERVER_METHOD_UNPACK        4

int main(int argc, char **argv){
	int argumentCount;
	struct argument *currentArg = NULL;
	fannypack_crypt_t cryptMethod = FANNYPACK_CRYPT_AES128;
	uint8_t serverMethod = 0;
	uint8_t startServer = 0;
	char dataPath[256], dataOutput[256], f1Data[16], f2Data[16];
	pthread_t *enqThread = NULL;

	fannypack_version_t v;
	uint8_t *dataPackage = NULL;
	uint8_t *dataResult = NULL;
	uint32_t packageLength = 0;
	int dataRead, fannypackResult, currentOffset, dataRemainning;
	FILE *inputFile = NULL, *outputFile = NULL;

	int udpSocket = -1, tcpSocket, clientSocket;
	struct sockaddr_in serverAddr;
	struct sockaddr_in clientAddr;
	socklen_t clientLength;
	struct tcp_package_data tcpPackage;
	struct tcp_reply tcpReply;

	dataPath[0] = '\0'; // empty string on output
	arguments = parseTheArguments(argc, argv, &argumentCount);
	if(argumentCount < 1) // we don't work with no arguments
		return printHelp(RESULT_BAD_ARGUMENTS, "Missing arguments");

	if((strcmp(arguments->value, "-h") == 0) || (strcmp(arguments->value, "--help") == 0)) // just print the help
		return printHelp(0, NULL);

	// define default version
	v.major = 0;
	v.minor = 0;
	v.revision = 0;
	v.user = 0;

	currentArg = arguments;
	while(currentArg){
		if(currentArg->value == NULL){
			return printHelp(RESULT_BAD_ARGUMENTS, "Bad formatted argument");
		}else if(strcmp(currentArg->value, "-f") == 0){
			// create package method: file
			serverMethod = SERVER_METHOD_FILE;
			if(currentArg->option == NULL)
				return printHelp(RESULT_BAD_ARGUMENTS, "Bad formatted arguments \'-f\'");

			strcpy(dataPath, currentArg->option);
		}else if(strcmp(currentArg->value, "-d") == 0){
			// create package method: directory
			serverMethod = SERVER_METHOD_DIR;
			if(currentArg->option == NULL)
				return printHelp(RESULT_BAD_ARGUMENTS, "Bad formatted arguments \'-d\'");

			strcpy(dataPath, currentArg->option);
		}else if(strcmp(currentArg->value, "-p") == 0){
			// create package method: server
			serverMethod = SERVER_METHOD_SERVER;
			startServer = 1;
			if(currentArg->option == NULL)
				return printHelp(RESULT_BAD_ARGUMENTS, "Bad formatted arguments \'-s\'");

			strcpy(dataPath, currentArg->option);
		}else if(strcmp(currentArg->value, "-u") == 0){
			// create package method: server
			serverMethod = SERVER_METHOD_UNPACK;
			startServer = 0;
			if(currentArg->option == NULL)
				return printHelp(RESULT_BAD_ARGUMENTS, "Bad formatted arguments \'-u\'");

			strcpy(dataPath, currentArg->option);

			dataOutput[0] = '\0'; // initialize with same folder as execution
			if(currentArg->next){
				// Output was provided
				if(!currentArg->next->value && (currentArg->next->option != NULL)){
					strcpy(dataOutput, currentArg->next->option);

					currentArg = currentArg->next; // skip one argument
				}
			}
		}else if(strcmp(currentArg->value, "-o") == 0){
			if((serverMethod != SERVER_METHOD_FILE) && (serverMethod != SERVER_METHOD_DIR))
				return printHelp(RESULT_BAD_ARGUMENTS, "Bad argument. \'-o\' only available in combination with -f|-d");
			if(currentArg->option == NULL)
				return printHelp(RESULT_BAD_ARGUMENTS, "Bad formatted arguments \'-o\'");

			strcpy(dataOutput, currentArg->option);
		}else if(strcmp(currentArg->value, "--noenc") == 0){
			cryptMethod = FANNYPACK_CRYPT_NONE;
		}else if(strcmp(currentArg->value, "-s") == 0){
			startServer = 1;
		}else if(strcmp(currentArg->value, "-v") == 0){
			if(currentArg->option == NULL)
				return printHelp(RESULT_BAD_ARGUMENTS, "Bad formatted version. Use \'M.m-R\'");

			v = FANNYPACK_ParseVersion(currentArg->option);
		}else{
			return printHelp(RESULT_BAD_ARGUMENTS, "Unknown argument \'%s\'", currentArg->value);
		}

		currentArg = currentArg->next;
	}

	free(arguments);

	if((serverMethod != SERVER_METHOD_SERVER) && (!startServer) && (dataPath[0] == '\0')){
		// we are generating a fannypack package, but there's no output for the data
		return printHelp(RESULT_BAD_ARGUMENTS, "No valid output for the generated package");
	}

	if(serverMethod == SERVER_METHOD_UNPACK){
		// Get package data and unpack
		FANNYPACK_DefineKey(theKey);

		dataRead = FANNYPACK_ChunksExtractData(TEMP_DATA_FILE, dataPath, CHUNK_SIZE);
		if(dataRead < 0){
			printf("Error writing result data. Error: %d\n", dataRead);
			return dataRead;
		}

		// Try extract archiver data
		dataRead = FANNYPACK_ChunksWriteArchiverEntries(TEMP_DATA_FILE, dataOutput, CHUNK_SIZE);
		if(dataRead == FANNYPACK_ERROR_NOT_ARCHIVER){
			// this is a file, copy temp file to output

			if(dataOutput[0] == '\0')// this is just a folder include a default file name
				strcpy(dataOutput, DEFAULT_FILE_NAME);
			else
				rmdir(dataOutput);

			if(rename(TEMP_DATA_FILE, dataOutput) < 0){
				return printHelp(errno, "Error creating destination file. Message: %s", strerror(errno));
			}
		}

		printf("Fannypack data unpacked in \'%s\'\n", dataOutput);
		return 0;
	}

	if(startServer){
		printf("Starting enquiry UDP server...\n");
		g_running = 0;

		enqThread = (pthread_t *)malloc(sizeof(pthread_t));
		if(pthread_create(enqThread, NULL, enquiryServer, (void *)&udpSocket) != 0)
			return printHelp(RESULT_ERROR_THREAD, "Error creating the thread. Message: %s", strerror(errno));

		printf("Waiting for socket to be created...");
		while(g_running == 0){
			printf(".");
			usleep(10000);
		}

		printf("\n");
		if(g_running < 0){
			stopServer(udpSocket, enqThread);
			return printHelp(g_running, "Error on UDP thread");
		}
	}

	// Arguments defined create data or server
	if(serverMethod == SERVER_METHOD_DIR){
		printf("Creating package from folder in \'%s\'...\n", dataPath);

		dataPackage = FANNYPACK_CreateDataArchiver(dataPath, "", &packageLength);
		if(!dataPackage){
			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_BAD_INPUT_FILE, "Couldn't open path \'%s\'", dataPath);
		}
	}else if(serverMethod == SERVER_METHOD_FILE){
		inputFile = fopen(dataPath, "r");

		printf("Creating package from file in \'%s\'...\n", dataPath);
		if(!inputFile){
			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_BAD_INPUT_FILE, "Couldn't open file on \'%s\'", dataPath);
		}

		fseek(inputFile, 0L, SEEK_END);
		packageLength = ftell(inputFile);
		rewind(inputFile);

		dataPackage = malloc(packageLength);
		if(!dataPackage){
			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_STACK_OVERFLOW, "Couldn't allocate memory to process data");
		}

		dataRead = fread(dataPackage, 1, packageLength, inputFile);
		fclose(inputFile);

		if(dataRead != packageLength){
			fitDataSize(packageLength, f1Data);
			fitDataSize(dataRead, f2Data);

			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_BAD_INPUT_FILE, "Mismatch between data size (%sB) and data read (%sB)", f1Data, f2Data);
		}
	}

	if(serverMethod != SERVER_METHOD_SERVER){
		fitDataSize(packageLength, f1Data);
		printf("Data is ready, original data length: %sB\n", f1Data);

		FANNYPACK_DefineKey(theKey);
		fannypackResult = FANNYPACK_CreatePackageStrapHeader(NULL, v, cryptMethod, dataPackage, packageLength, &dataResult);

		if(dataPackage)
			free(dataPackage);

		if(fannypackResult <= 0){
			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_ERROR_CREATING_PACKAGE, "Bad package creation. Result: %d", fannypackResult);
		}

		// check if data output is set
		fitDataSize(fannypackResult, f1Data);
		printf("Fannypack package created, length: %s\n", f1Data);
		if(dataOutput[0] != '\0'){
			// there's a path on dataPath, save the data
			outputFile = fopen(dataOutput, "w");
			if(!outputFile){
				stopServer(udpSocket, enqThread);
				return printHelp(RESULT_BAD_OUTPUT_FILE, "Couldn't open file \'%s\' to write. Message: %s", dataOutput, strerror(errno));
			}

			dataRead = fwrite(dataResult, 1, fannypackResult, outputFile);
			fflush(outputFile);
			fclose(outputFile);

			if(dataRead != fannypackResult){
				fitDataSize(dataRead, f1Data);
				fitDataSize(fannypackResult, f2Data);

				free(dataPackage);
				stopServer(udpSocket, enqThread);
				return printHelp(RESULT_BAD_INPUT_FILE, "Mismatch between data size (%sB) and data written (%sB)", f1Data, f2Data);
			}
		}

		printf("Resulting package written to \'%s\'\n", dataOutput);
	}else{
		printf("Reading fannypack data from \'%s\'...\n", dataPath);

		inputFile = fopen(dataPath, "r");
		if(!inputFile){
			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_BAD_INPUT_FILE, "Couldn't open file \'%s\' to read. Message: %s", dataPath, strerror(errno));
		}

		fseek(inputFile, 0L, SEEK_END);
		packageLength = ftell(inputFile);
		rewind(inputFile);

		dataResult = malloc(packageLength);
		if(!dataResult){
			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_STACK_OVERFLOW, "Couldn't allocate memory to process data");
		}

		fannypackResult = fread(dataResult, 1, packageLength, inputFile);
		fclose(inputFile);

		if(fannypackResult != packageLength){
			fitDataSize(packageLength, f1Data);
			fitDataSize(fannypackResult, f2Data);

			free(dataPackage);
			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_BAD_INPUT_FILE, "Mismatch between data size (%sB) and data read (%sB)", f1Data, f2Data);
		}

		fitDataSize(fannypackResult, f1Data);
		printf("Fannypack package readen, length: %s\n", f1Data);
	}

	if(startServer){
		g_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(g_serverSocket < 0){
			free(dataResult);

			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_BAD_TCP_SERVER, "Error creating TCP server");
		}

		bzero(&serverAddr, sizeof(struct sockaddr_in));
		serverAddr.sin_family = AF_INET;
		serverAddr.sin_port = htons(FANNYPACK_TCP_SERVER_PORT);
		serverAddr.sin_addr.s_addr = INADDR_ANY;
		bzero(&clientAddr, sizeof(struct sockaddr_in));

		printf("Binding TCP socket..\n");
		if(bind(g_serverSocket, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in)) != 0){
			free(dataResult);

			stopServer(udpSocket, enqThread);
			return printHelp(RESULT_BAD_TCP_SERVER, "Error binding socket. Message: %s", strerror(errno));
		}

		listen(g_serverSocket, 5);
		printf("Waiting for client\n");
		clientSocket = accept(g_serverSocket, (struct sockaddr *)&clientAddr, &clientLength);
//		if(clientAddr.sin_family != AF_INET){
//			free(dataResult);
//
//			stopServer(udpSocket, enqThread);
//			return printHelp(RESULT_BAD_TCP_SERVER, "No support for IPv6");
//		}
		printf("Connected to \'%s\', sending data...\n", inet_ntoa(clientAddr.sin_addr));

		currentOffset = 0;
		dataRemainning = fannypackResult;

		// Use one and only transaction ID
		do{
			tcpPackage.transactionID = (uint32_t)random();
		}while(tcpPackage.transactionID == 0);


		while(dataRemainning){
			tcpPackage.totalSize         = fannypackResult;
			tcpPackage.remainningData    = dataRemainning;
			tcpPackage.packageOffset     = currentOffset;
			if(dataRemainning > FANNYPACK_TCP_DATA_LENGTH)
				tcpPackage.packageLength = FANNYPACK_TCP_DATA_LENGTH;
			else
				tcpPackage.packageLength = dataRemainning;

			memcpy(tcpPackage.data, (dataResult + currentOffset), tcpPackage.packageLength);
			dataRead = send(clientSocket, (void *)&tcpPackage, FANNYPACK_TCP_TOTAL_LENGTH, 0);
			if(dataRead != FANNYPACK_TCP_TOTAL_LENGTH){
				fitDataSize(FANNYPACK_TCP_TOTAL_LENGTH, f1Data);
				fitDataSize(dataRead, f2Data);
				printf("TCP package length mismatch, package length: %s, data send: %s\n", f1Data, f2Data);
			}

			dataRead = recv(clientSocket, (void *)&tcpReply, sizeof(struct tcp_reply), 0);
			if(dataRead == sizeof(struct tcp_reply)){
				if(tcpReply.magicCookie == CONF_FANNYPACK_MAGIC){
					if(tcpReply.replyCode == FANNYPACK_TCP_REPLY_SUCCESS){
						if(tcpReply.transactionID == tcpPackage.transactionID){
							// message was a SUCCESS, send next chunk, otherwise send the same feces
							currentOffset += tcpPackage.packageLength;
							dataRemainning -= tcpPackage.packageLength;
						}
					}
				}
			}
		}

		// close the sockets
		close(clientSocket);
		close(g_serverSocket);

		stopServer(udpSocket, enqThread);
	}

	printf("Job executed, see'ya!\n");
	return 0;
}

void stopServer(int socketNum, pthread_t *thread){
	g_running = 0;

	if(socketNum > 0)
		close(socketNum);

	if(thread)
		pthread_join((*thread), NULL);
}

void *enquiryServer(void *argument){
	int *enqSocket = (int *)argument;
	int result, dataTransmit;
	socklen_t clientLength;
	struct timeval recvTimeout;
	struct sockaddr_in serverAddr, clientAddr;
	struct udp_enquiry_data request, reply;
	struct ifaddrs *ifaddr;
	uint32_t ourIP = 0;

	// Start by getting our own IP
	if(getifaddrs(&ifaddr) == -1){
		g_running = errno;
		pthread_exit((void *)&g_running);
	}

	for(struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
		if(ifa->ifa_addr == NULL)
			continue;

		if(strcmp(ifa->ifa_name, "lo") == 0) // we don't care about loopback
			continue;

		if(ifa->ifa_addr->sa_family != AF_INET) // we don't other than IPv4
			continue;

		// this is probably our network IP. hope so
		ourIP = (uint32_t)(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr.s_addr);
		break;
	}

	freeifaddrs(ifaddr);

	if(!ourIP){
		// couldn't figure our IP out
		g_running = RESULT_UDP_ERROR_OUR_IP;
		pthread_exit((void *)&g_running);
	}

	(*enqSocket) = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if((*enqSocket) < 0){
		g_running = RESULT_UDP_ERROR_SOCKET;
		pthread_exit((void *)&g_running);
	}

	recvTimeout.tv_sec = 10;
	recvTimeout.tv_usec = 0;

	if(setsockopt((*enqSocket), SOL_SOCKET, SO_RCVTIMEO, (void *)&recvTimeout, sizeof(struct timeval)) < 0){
		g_running = RESULT_UDP_ERROR_SOCKET;
		pthread_exit((void *)&g_running);
	}

	// bind socket and wait for enquiry
	bzero(&serverAddr, sizeof(struct sockaddr_in));
	bzero(&clientAddr, sizeof(struct sockaddr_in));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	serverAddr.sin_port = htons(FANNYPACK_UDP_SERVER_PORT);

	result = bind((*enqSocket), (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in));
	if(result < 0){
		g_running = errno;
		pthread_exit((void *)&g_running);
	}

	g_running = 1;
	while(g_running){
		dataTransmit = recvfrom((*enqSocket), &request, sizeof(struct udp_enquiry_data), 0, (struct sockaddr *)&clientAddr, &clientLength);
		if(dataTransmit < 0){
			// probably no one enquiried us, just continue
			continue;
		}

		if(dataTransmit == sizeof(struct udp_enquiry_data)){
			clientAddr.sin_addr.s_addr = request.howsAsking;
			clientAddr.sin_port = htons(FANNYPACK_UDP_CLIENT_PORT);
			printf("Received UDP enquiry from %s:%d\n", inet_ntoa(clientAddr.sin_addr), htons(clientAddr.sin_port));


			// only process fannypack's udp_enquiry_data
			if((request.queryType == FANNYPACK_UDP_REQUEST) || (request.replyType == FANNYPACK_UDP_WE_ARE_CLIENT)){
				// a fine request from a fellow client, say that we are a server and running
				reply.queryType = FANNYPACK_UDP_REPLY;
				reply.howsAsking = request.howsAsking;
				reply.howsReplying = ourIP;
				if(g_serverSocket > 0) // server is running and waiting
					reply.replyType = FANNYPACK_UDP_WE_ARE_SERVER;
				else
					reply.replyType = FANNYPACK_UDP_WE_ARE_IDLE;

				// Send reply to pergunter
				printf("Replying to %s on %d\n", inet_ntoa(clientAddr.sin_addr), htons(clientAddr.sin_port));
				sendto((*enqSocket), &reply, sizeof(struct udp_enquiry_data), 0, (struct sockaddr *)&clientAddr, sizeof(struct sockaddr_in));
			}else{
				printf("Bad UDP request\n");
			}
		}
	}

	pthread_exit((void *)&g_running);
}

static void fitDataSize(double value, char *wat){
	if(value > 1048576.0)
		sprintf(wat, "%.2fM", (value / 1048576.0));
	else if(value > 1024.0)
		sprintf(wat, "%.2fK", (value / 1024.0));
	else
		sprintf(wat, "%.2f", value);
}

static void fitTime(double value, char *wat){
	int hours, mins, secs;

	if(value > 3600)
		hours = value / 3600;
	else
		hours = 0;

	value -= (hours * 3600);
	if(value > 60)
		mins = value / 60;
	else
		mins = 0;

	value -= (mins * 60);
	secs = (int)value;

	sprintf(wat, "%02d:%02d:%02d", hours, mins, secs);
}

static int ConnectSocket(int *s, struct sockaddr_in *addr){
	int sockLength, result = 0;

	(*s) = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if((*s) < 0){
		printf("Something funky about the socket, result: %d\n", (*s));
		return result;
	}

	// Try to set a timeout on the send
	struct timeval timeout;
	timeout.tv_sec = 30;
	timeout.tv_usec = 0;

	result = setsockopt((*s), SOL_SOCKET, SO_RCVTIMEO, (void *)&timeout, sizeof(struct timeval));
	if (result < 0){
		printf("Something funky about setting timeout for the socket, result: %d\n", result);
		return -4;
	}

	sockLength = sizeof(struct sockaddr_in);
	bzero(addr, sizeof(struct sockaddr_in));
	inet_aton("10.10.0.11", &(addr->sin_addr));
	addr->sin_port = htons(3230);
	addr->sin_family = AF_INET;

	result = connect((*s), (struct sockaddr *)addr, sizeof(struct sockaddr_in));
	if(result < 0){
		printf("Not connected :(\nIs the server running? Ping that bitch. Result: %d\n", result);
		return -5;
	}

	return 0;
}

int printHelp(int rtnValue, char *message, ...){
	va_list l;

	if(message){
		va_start(l, message);
		printf("\033[1;31mERROR:\033[0m ");
		vprintf(message, l);
		va_end(l);
		printf("\n");
	}

	// Print default help message:
	printf("Usage:\tfannypack [-h|--help] [--noenc] [-s] [-v] <data version> {-f|-d} <input> {-o} <output>\n");
	printf("\tfannypack [-h|--help] {-p} <path>\n");
	printf("\tfannypack [-h|--help] {-u} <package> [output]\n");
	printf("\t  -h|--help: Show this message\n");
	printf("\t  --noenc:   Output fannypack package won't be encrypted\n");
	printf("\t  -s:        Start server after package generation. Makes \'-o\' optional\n");
	printf("\t  -v:        Version of the data (format vM.m-R), with not informed v0.0\n");
	printf("\t  -f:        Create fannypack package from file in <input>\n");
	printf("\t  -d:        Create fannypack package from folder entries in <input>\n");
	printf("\t  -o:        Save resulting package in <output>\n");
	printf("\t  -p:        Start server with fannypack package in <path>\n");
	printf("\t  -u:        Unpack fannypack package <package> in <output>\n");

	return rtnValue;
}

//int main(int argc, char **argv){
//	fannypack_version_t v;
//	char *theFolder = NULL;
//	uint8_t *dataPackage;
//	uint8_t *dataResult;
//	uint32_t packageLength = 0;
//	int argCount, result, fannypackResult, dataSend, dataRecv, dataOffset, amountToSend;
//	char theRecvData[16];
//	struct timeval totalBegin, totalEnd, totalCurrent;
//
//	struct ifaddrs *ifaddr;
//	uint32_t ourIP = 0;
//
//	// Start by getting our own IP
//	if(getifaddrs(&ifaddr) == -1){
//		return -1;
//	}
//
//	for(struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
//		if(ifa->ifa_addr == NULL)
//			continue;
//
//		if(strcmp(ifa->ifa_name, "lo") == 0) // we don't care about loopback
//			continue;
//
//		if(ifa->ifa_addr->sa_family != AF_INET) // we don't other than IPv4
//			continue;
//
//		// this is probably our network IP. hope so
//		ourIP = (uint32_t)(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr.s_addr);
//	}
//
//	struct argument *theArgs = parseTheArguments(argc, argv,&argCount);
//	if(!theArgs){
//		printf("You need args for this to work \'-s {folder you gonna send baby}\'\n");
//		return -1;
//	}
//
//	for(int idx = 0; idx < argCount; idx++){
//		if(strcmp(theArgs[idx].option , "-s") == 0){
//			// This is it!
//			theFolder = theArgs[idx].value;
//		}
//	}
//
//	if(!theFolder){
//		printf("You need A FOLDER to work with....mula! \'-s {folder you gonna send baby}\'\n");
//		return -2;
//	}
//
//	FANNYPACK_DefineKey(theKey);
//	dataPackage = FANNYPACK_CreateDataArchiver(theFolder, "", &packageLength);
//	fannypackResult = FANNYPACK_CreatePackageStrapHeader(NULL, v, FANNYPACK_CRYPT_AES128, dataPackage, packageLength, &dataResult);
//
//	free(theArgs);
//	free(dataPackage);
//	if(fannypackResult <= 0){
//		printf("Either no data or something funky. Fannypack response was: %d\n", result);
//		return -3;
//	}
//
//	// Send this lovely package over the interwebs
//	int connectionSocket, sockLength;
//	struct sockaddr_in addrClient;
//
//	result = ConnectSocket(&connectionSocket, &addrClient);
//	if(result)
//		return result;
//
//
//	double transferRate[128], timeSum;
//	int transferIdx = 0;
//	struct timeval begin, end;
//	double timeTaken;
//	double bytesPerSec;
//	char value1[64], value2[64], value3[64], value4[64];
//
//	uint32_t totalData, remainningData, offsetData, dataLength, iT;
//	uint8_t sendBuffer[2048], recvData[16];
//
//	bzero(transferRate, sizeof(transferRate));
//
//	// To get the total transfer time
//	gettimeofday(&totalBegin, NULL);
//
//	// start sending the package
//	offsetData = 0;
//	remainningData = fannypackResult;
//	dataLength = 4;
//	*((uint32_t *)(sendBuffer + 8)) = offsetData;
//	if(remainningData > (2048 - 1 - 12)){	// free data for package
//		*((uint32_t *)(sendBuffer)) = (2048 - 1 - 12);
//		memcpy((sendBuffer + 12), (dataResult + offsetData), (2048 - 1 - 12));
//		offsetData += (2048 - 1 - 12);
//		remainningData -= (2048 - 1 - 12);
//		dataLength += (2048 - 1 - 12) + 4;
//	}else{
//		*((uint32_t *)(sendBuffer)) = remainningData;
//		memcpy((sendBuffer + 12), (dataResult + offsetData), (2048 - 1 - 12));
//		offsetData += remainningData;
//		remainningData -= remainningData;
//		dataLength += remainningData + 4;
//	}
//	*((uint32_t *)(sendBuffer + 4)) = remainningData;
//	dataLength += 4;
//	sendBuffer[dataLength++] = 0xFB;
//
//	gettimeofday(&begin, NULL);
//	while(remainningData){
//
//		dataSend = send(connectionSocket, sendBuffer, dataLength, 0);
//		if(errno){
//			// Something wrong with the transmission
////			if(errno){
//				printf("Error in socket, restarting...\n");
//				close(connectionSocket);
//				result = ConnectSocket(&connectionSocket, &addrClient);
//				if(result)
//					return result;
//
//				continue;
////			}
//		}else{
//			dataRecv = recv(connectionSocket, recvData, 16, 0);
//			if(dataRecv <= 0){
//				// probably a dead socket
//				continue;
//			}else{
//				recvData[dataRecv] = '\0';
//				// Calculate the transfer rate
//				gettimeofday(&end, NULL);
//
//				timeTaken = (end.tv_sec - begin.tv_sec) + ((end.tv_usec / 1000000.0) - (begin.tv_usec / 1000000.0));
//				transferRate[transferIdx++] = dataSend / timeTaken;
//				transferIdx &= 0x7F;
//
//				timeSum = 0;
//				for(iT = 0; iT < 128; iT++){
//					if(transferRate[iT] == 0)
//						break;
//					timeSum += transferRate[iT];
//				}
//				timeSum /= (double)iT;
//
//				gettimeofday(&totalCurrent, NULL);
//
//				fitDataSize(remainningData, value1);
//				fitDataSize(timeSum, value2);
//				fitTime((remainningData / timeSum), value3);
//				fitTime((totalCurrent.tv_sec - totalBegin.tv_sec), value4);
//				printf("Received: \'%s\'. Remainning: %sB. Transfer Rate: %sBps. ETA: %s. Current: %s\n", recvData, value1, value2, value3, value4);
//				if(strcmp(recvData, "DEUBAUM") == 0){
//					// GOOOOOOOOD package, send next chunk
//					dataLength = 4;
//					*((uint32_t *)(sendBuffer + 8)) = offsetData;
//					if(remainningData > (2048 - 1 - 12)){	// free data for package
//						*((uint32_t *)(sendBuffer)) = (2048 - 1 - 12);
//						memcpy((sendBuffer + 12), (dataResult + offsetData), (2048 - 1 - 12));
//						offsetData += (2048 - 1 - 12);
//						remainningData -= (2048 - 1 - 12);
//						dataLength += (2048 - 1 - 12) + 4;
//					}else{
//						*((uint32_t *)(sendBuffer)) = remainningData;
//						memcpy((sendBuffer + 12), (dataResult + offsetData), (2048 - 1 - 12));
//						offsetData += remainningData;
//						remainningData -= remainningData;
//						dataLength += remainningData + 4;
//					}
//					*((uint32_t *)(sendBuffer + 4)) = remainningData;
//					dataLength += 4;
//					sendBuffer[dataLength++] = 0xFB;
//
//					gettimeofday(&begin, NULL);
//				}
//			}
//		}
//	}
//
//	gettimeofday(&totalEnd, NULL);
//	fitTime((totalEnd.tv_sec - totalBegin.tv_sec), value1);
//	printf("Package transmitted in %s. Bye!\n", value1);
//
///*
//	dataOffset = 0;
//	memset(transferRate, 0, (sizeof(double) * 128));
//	while(fannypackResult > 0){
//		gettimeofday(&begin, NULL);
//		if(fannypackResult > 1024)
//			amountToSend = 1024;
//		else
//			amountToSend = fannypackResult;
//
//		do{
//			dataSend = send(connectionSocket, (dataResult + dataOffset), amountToSend, 0);
//			if(errno){
//				// Socket is broken, try to reconnect
//				close(connectionSocket);
//
//				result = ConnectSocket(&connectionSocket, &addrClient);
//				if(result)
//					return result;
//
//				continue;
//			}
//
//			if((dataSend >= 0) && (dataSend != amountToSend)){
//				printf("Bad bad socket....\n");
//				continue;
////				close(connectionSocket);
////				return -6;
//			}
//		}while(dataSend < 0);
//
//		fannypackResult -= dataSend;
//		dataOffset += dataSend;
//		dataRecv = recv(connectionSocket, theRecvData, 6, 0);
//		gettimeofday(&end, NULL);
//
//		// send might get lost, if so, don't care
//		if(dataRecv > 0){
//			// Calculate the amount of time taken
//			timeTaken = (unsigned int)(((end.tv_sec - begin.tv_sec) * 1000000) + ((end.tv_usec - begin.tv_usec)));
//			if(timeTaken > 0)
//				transferRate[transferIdx++] = dataSend / (double)timeTaken;
//			else
//				transferRate[transferIdx++] = 10485760.0; // 10MBps, can't be faster than this!
//			transferIdx &= 0x1F;
//
//			timeSum = 0;
//			int tI;
//			for(tI = 0; tI < 128; tI++){
//				if(transferRate[tI] == 0)
//					break;
//
//				timeSum += transferRate[tI];
//			}
//
//			if(tI > 0)
//				timeSum /= (double)tI; // divide by 32
//
//			bytesPerSec = timeSum * 1000000.0;
//
//			if(memcmp(theRecvData, "BAUMS", 5) == 0){
//				printf("Deu bão no pacotin, faltam: %d, Rate: %s\n", fannypackResult, getTransferRate(bytesPerSec));
//			}else{
//				theRecvData[6] = '\0';
//				printf("Veio isso mano....sei lá: %s\n", theRecvData);
//				close(connectionSocket);
//				return -7;
//			}
//		}
//	}
//
//	close(connectionSocket);
//
//	if(result >= 0){
//		// Test chunks
//		fp = fopen("thePackage", "w");
//		fwrite(dataResult, 1, result, fp);
//		fclose(fp);
//
//		result = FANNYPACK_ChunksExtractData("theData", "thePackage", 512);
//		if(result > 0){
//			result = FANNYPACK_ChunksWriteArchiverEntries("theData", "../grobus", 512);
//		}
//
//		// Test whole package
////		dataPackage = FANNYPACK_ExtractData(dataResult, &result);
////		result = FANNYPACK_WriteArchiverEntries("../tulisba", dataPackage);
////		free(dataPackage);
//
//		free(dataResult);
//		if(result != FANNYPACK_ERROR_SUCCESS){
//			return result;
//		}
//	}
//*/
//
//	return 0;
//}
