/*
 * client.h
 *
 *  Created on: Jan. 9, 2021
 *      Author: otavio
 */

#include "libfannypack.h"
#include "argParser.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include <errno.h>

int main(int argc, char **argv){
	char dataOutput[256];
	int broadcast = 1;
	int s;
	struct sockaddr_in clientAddr, serverAddr, senderAddr;
	socklen_t senderLength;
	struct timeval timeout;
	int tcpServerOn = 0, dataTransmit, successChunk, remainningData, totalLength;

	struct udp_enquiry_data enqData, enqResp;
	struct tcp_package_data tcpData;
	struct tcp_reply tcpReply;

	uint8_t *packageData = NULL;
	uint32_t expectedID = 0;

	struct ifaddrs *ifaddr;
	uint32_t ourIP = 0;

	// get the arguments
	int argCount;
	struct argument *arg = parseTheArguments(argc, argv, &argCount);
	if((!arg) || (!arg->option)){
		printf("Bad input. Usage client <data output>\n");
		return -1;
	}
	strcpy(dataOutput, arg->option);
	free(arg);

	// Start by getting our own IP
	if(getifaddrs(&ifaddr) == -1){
		printf("Error getting interface IP address. Message: %s\n", strerror(errno));
		return -2;
	}

	for(struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
		if(ifa->ifa_addr == NULL)
			continue;

		if(strcmp(ifa->ifa_name, "lo") == 0) // we don't care about loopback
			continue;

		if(ifa->ifa_addr->sa_family != AF_INET) // we don't other than IPv4
			continue;

		// this is probably our network IP. hope so
		ourIP = (uint32_t)(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr.s_addr);
		break;
	}

	freeifaddrs(ifaddr);

	if(!ourIP){
		// couldn't figure our IP out
		printf("Could not define the interface IP.\n");
		return -3;
	}

	// Create UDP socket and wait to be available
	s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(s < 0){
		printf("Error creating socket. Message: %s\n", strerror(errno));
		return -4;
	}

	printf("Adding broadcast capability...\n");
	if(setsockopt(s, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(int)) < 0){
		close(s);

		printf("Error enabling broadcast. Message: %s\n", strerror(errno));
		return -5;
	}

	printf("Setting send timeout for UDP socket\n");
	timeout.tv_sec = 5;
	timeout.tv_usec = 0;
	if(setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, (void *)&timeout, sizeof(struct timeval)) < 0){
		close(s);

		printf("Error setting UDP socket timeout. Message: %s\n", strerror(errno));
		return -6;
	}

	bzero(&clientAddr, sizeof(struct sockaddr_in));
	bzero(&serverAddr, sizeof(struct sockaddr_in));

	clientAddr.sin_family = AF_INET;
	clientAddr.sin_port = htons(FANNYPACK_UDP_CLIENT_PORT);
	clientAddr.sin_addr.s_addr = INADDR_ANY;

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(FANNYPACK_UDP_SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_BROADCAST;

	printf("Waiting for server to be up...\n");
	if(bind(s, (struct sockaddr *)&clientAddr, sizeof(struct sockaddr_in)) < 0){
		close(s);

		printf("Error binding socket to client port. Message: %s\n", strerror(errno));
		return -7;
	}

	enqData.queryType = FANNYPACK_UDP_REQUEST;
	enqData.howsAsking = ourIP; // don't care
	enqData.howsReplying = 0;
	enqData.replyType = 0;

	while(!tcpServerOn){
		dataTransmit = sendto(s, (void *)&enqData, sizeof(struct udp_enquiry_data), 0, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in));
		if(dataTransmit != sizeof(struct udp_enquiry_data)){
			printf("UDP data length mismatch\n");
		}

		dataTransmit = recvfrom(s, (void *)&enqResp, sizeof(struct udp_enquiry_data), 0, (struct sockaddr *)&senderAddr, &senderLength);
		if(dataTransmit == sizeof(struct udp_enquiry_data)){
			// This is what we want
			if(enqResp.queryType == FANNYPACK_UDP_REPLY){
				if(enqResp.replyType == FANNYPACK_UDP_WE_ARE_SERVER){
					// The server is up and GOOOOOD to go
					serverAddr.sin_addr.s_addr = enqResp.howsReplying;
					tcpServerOn = 1;
				}
			}
		}
	}

	// TCP server is ON, connect and receive that data safada
	close(s);
	serverAddr.sin_port = htons(FANNYPACK_TCP_SERVER_PORT);

	FILE *theResult;
	theResult = fopen(dataOutput, "w");
	if(!theResult){
		close(s);

		printf("Error opening \'%s\' to receive data\n", dataOutput);
		return -8;
	}

	s = -1;
	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(s < 0){
		printf("Error creating TCP socket. Message: %s\n", strerror(errno));
		return -9;
	}

	if(connect(s, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in)) != 0){
		close(s);

		printf("Error connecting to server at \'%s\'. Message: %s\n", inet_ntoa(serverAddr.sin_addr), strerror(errno));
	}

	// Receive THE Dataaaaaaaaaaaaaaaaaaaaaaaaaaa, baby
	tcpReply.magicCookie = CONF_FANNYPACK_MAGIC;
	successChunk = 0;
	remainningData = 0;
	totalLength = 0;
	do{
		dataTransmit = recv(s, (void *)&tcpData, sizeof(struct tcp_package_data), 0);
		if(dataTransmit == sizeof(struct tcp_package_data)){
			if(!totalLength){
				totalLength = tcpData.totalSize;
				expectedID = tcpData.transactionID;
				tcpReply.transactionID = tcpData.transactionID;
				remainningData = tcpData.totalSize;

				successChunk = 1;
			}else{
				if(tcpData.totalSize == totalLength){
					if(tcpData.transactionID == expectedID)
						successChunk = 1;
				}
			}

			if(successChunk){
				if(!packageData)
					packageData = (uint8_t *)malloc(tcpData.totalSize);

				memcpy((packageData + tcpData.packageOffset), tcpData.data, tcpData.packageLength);
				remainningData -= tcpData.packageLength;

				tcpReply.replyCode = FANNYPACK_TCP_REPLY_SUCCESS;
				successChunk = 0;
			}else{
				tcpReply.replyCode = FANNYPACK_TCP_REPLY_FAIL;
			}
		}

		dataTransmit = send(s, (void *)&tcpReply, sizeof(struct tcp_reply), 0);
		if(dataTransmit < 0){
			printf("Bad bad socket...close\n");
			close(s);

			return -10;
		}
	}while(remainningData);

	close(s);
	if(fwrite(packageData, 1, totalLength, theResult) != totalLength){
		printf("Mismatch writing on result file \'%s\'\n", dataOutput);

		fclose(theResult);
		return -11;
	}else{
		printf("Fannypack package successully written .\n");
	}

	fclose(theResult);
	return 0;
}
