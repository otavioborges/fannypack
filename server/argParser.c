/*
 * argParser.c
 *
 *  Created on: Dec 19, 2020
 *      Author: otavio
 */

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "argParser.h"

struct argument *parseTheArguments(int argc, char **argv, int *listLength){
	struct argument *rtnValue = NULL;
	struct argument *theList = NULL;

	(*listLength) = 0;
	for(int argIdx = 1; argIdx < argc; argIdx++){
		if(rtnValue == NULL){
			rtnValue = (struct argument *)malloc(sizeof(struct argument));
			theList = rtnValue;
		}else{
			theList->next = (struct argument *)malloc(sizeof(struct argument));
			theList = theList->next;
		}

		theList->option = NULL;
		theList->value = NULL;
		theList->next = NULL;
		(*listLength)++;

		if(argv[argIdx][0] == '-'){ // this is an option
			theList->value = (char *)malloc(sizeof(char) * strlen(argv[argIdx]));
			strcpy(theList->value, argv[argIdx]);

			if(argIdx == (argc - 1)){ // last option
				theList->option = NULL;
			}else{
				if(argv[argIdx + 1][0] != '-'){ // next value is a value not option
					theList->option = (char *)malloc(sizeof(char) * strlen(argv[argIdx + 1]));
					strcpy(theList->option, argv[argIdx + 1]);
					argIdx++; // skip loop for next value
				}else{
					theList->option = NULL;
				}
			}
		}else{
			theList->value = NULL;
			theList->option = (char *)malloc(sizeof(char) * strlen(argv[argIdx]));
			strcpy(theList->option, argv[argIdx]);
		}
	}

	return rtnValue;
}
