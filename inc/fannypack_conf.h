/*
 * fannypack_conf.h
 *
 *  Created on: Dec. 19, 2020
 *      Author: otavio
 */

#ifndef INC_FANNYPACK_CONF_H_
#define INC_FANNYPACK_CONF_H_

#ifndef CONF_LIB_VERSION
	#ifdef ARCH_ARM
		#define CONF_LIB_VERSION                        0x00000101
	#else
		#define CONF_LIB_VERSION                        0x00000100
	#endif
#endif

#define CONF_FANNYPACK_MAGIC                            0xC0FFEEAF
#define CONF_FANNYPACK_STRAP_HEADER_LENGTH              64U
#define CONF_FANNYPACK_NORMAL_HEADER_LENGTH             256U

#define CONF_ARCHIVER_MAGIC                             0xCAFED00D
#define CONF_ARCHIVER_ENTRY_LENGTH                      128U
#define CONF_ARCHIVER_ENTRY_MASK                        0xFFFFFF80U        // This mask clamps chunks into multiples of Entries
#define CONF_ARCHIVER_ENTRY_DIVIDER                     7U
#define CONF_ARCHIVER_HEADER_LENGTH                     16U

#define CONF_COMPRESS_LEVEL                             9    // Max compression, bitch! Save that data
#define CONF_MAX_CHUNK                                  512
#define CONF_FANNYPACK_COMPRESS_LEEWAY                  256

#define CONF_CRYPT_METHODS                              1

#define CONF_HEADER_UID_LENGTH                          4
#define CONF_HEADER_USER1_LENGTH                        8
#define CONF_HEADER_IV_LENGTH                           16
#define CONF_HEADER_DESC_LENGTH                         96
#define CONF_HEADER_USER2_LENGTH                        32
#define CONF_HEADER_RESERVED_LENGTH                     92

/* ERROR TYPES */
#define FANNYPACK_ERROR_SUCCESS                          0
#define FANNYPACK_ERROR_WRONG_DATA                      -1
#define FANNYPACK_ERROR_ILLEGAL_CRYPT_METHOD            -2
#define FANNYPACK_ERROR_NOT_IMPLEMENTED                 -3
#define FANNYPACK_ERROR_NOT_A_FANNYPACK_BRO             -4
#define FANNYPACK_ERROR_THREAD_ERROR                    -5
#define FANNYPACK_ERROR_INIT_ZLIB                       -6
#define FANNYPACK_ERROR_COMPRESSING                     -7
#define FANNYPACK_ERROR_INVALID_PATH                    -8
#define FANNYPACK_ERROR_EACCES                          -9
#define FANNYPACK_ERROR_FILE_CREATION                   -10
#define FANNYPACK_ERROR_FILE_READING                    -11
#define FANNYPACK_ERROR_FILE_WRITING                    -12
#define FANNYPACK_ERROR_FILE_SIZE_MISMATCH              -13
#define FANNYPACK_ERROR_CHUNK_TOO_SMALL                 -14
#define FANNYPACK_ERROR_MEM_ERROR                       -15
#define FANNYPACK_ERROR_NOT_ARCHIVER                    -16

/* System call definitions:
 *     Different definitions to allow multi-arch
 *     Compilations.
 */

#define CONF_TARGET_LINUX                               // On your day-to-day computer
//#define CONF_TARGET_FATFS                             // For embedded systems

#ifdef CONF_TARGET_LINUX

// Include Linux system libraries
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#define CONF_ARCHIVER_DIR_ACCESS_FLAGS                  (S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)

// File operations
#define FILE_DIR_STRUCT                                 DIR *
#define FILE_DIR_ENTRY_STRUCT                           struct dirent *
#define FILE_FILE_STRUCT                                FILE *
#define FILE_STAT_STRUCT								struct stat

#define FILE_OPEN_READ(file, path)                      ((file = fopen(path, "r")) != NULL)
#define FILE_OPEN_WRITE(file, path)                     ((file = fopen(path, "w")) != NULL)
#define FILE_FLUSH(file)                                fflush(file)
#define FILE_CLOSE(file)								fclose(file)
#define FILE_DATA_READ(result, buffer, length, file)    result = fread(buffer, 1, length, file)
#define FILE_DATA_WRITE(result, buffer, length, file)   result = fwrite(buffer, 1, length, file)
#define FILE_DELETE(file)                               remove(file)
#define FILE_REWIND(file, offset)						fseek(file, offset, SEEK_SET);
#define FILE_CREATE_DIR(path)                           mkdir(path, CONF_ARCHIVER_DIR_ACCESS_FLAGS)
#define FILE_OPEN_DIR(dir, path)						((dir = opendir(path)) != NULL)
#define FILE_READ_DIR(dir, entry)						((entry = readdir(dir)) != NULL)
#define FILE_STAT(path, stats)                           stat(path, &stats)
#define FILE_CLOSE_DIR(dir)								closedir(dir)

#define FILE_ENTRY_NAME(entry)                          entry->d_name
#define FILE_IS_DIR(entry)                              (entry->d_type == DT_DIR)
#define FILE_GET_SIZE(stat)                             (stats.st_size)

#define MEMORY_MALLOC(size)                             malloc(size)
#define MEMORY_FREE(pointer)                            free(pointer)
#endif

#ifdef CONF_TARGET_FATFS

#include "ff.h"
#include "portable.h"

#define FILE_DIR_STRUCT                                 DIR
#define FILE_DIR_ENTRY_STRUCT                           FILINFO
#define FILE_FILE_STRUCT                                FIL
#define FILE_STAT_STRUCT                                FILINFO

#define FILE_OPEN_READ(file, path)                      (f_open(&file, path, FA_READ) == FR_OK)
#define FILE_OPEN_WRITE(file, path)                     (f_open(&file, path, (FA_CREATE_ALWAYS | FA_WRITE)) == FR_OK)
#define FILE_FLUSH(file)                                f_sync(&file)
#define FILE_CLOSE(file)                                f_close(&file)
#define FILE_DATA_READ(result, buffer, length, file)    f_read(&file, buffer, length, (UINT *)&result)
#define FILE_DATA_WRITE(result, buffer, length, file)   f_write(&file, buffer, length, (UINT *)&result)
#define FILE_DELETE(path)                               f_unlink(path)
#define FILE_REWIND(file, offset)                       f_lseek(&file, offset)
#define FILE_CREATE_DIR(path)                           f_mkdir(path)
#define FILE_OPEN_DIR(dir, path)                        (f_opendir(&dir, path) == FR_OK)
#define FILE_READ_DIR(dir, entry)                       (f_readdir(&dir, &entry) == FR_OK)
#define FILE_STAT(path, stat)                           f_stat(path, &stat)
#define FILE_CLOSE_DIR(dir)                             f_closedir(&dir)

#define FILE_ENTRY_NAME(entry)                          entry.fname
#define FILE_IS_DIR(entry)                              (entry.fattrib & AM_DIR)
#define FILE_GET_SIZE(stat)                             (stat.fsize)

#define MEMORY_MALLOC(size)                             pvPortMalloc(size)
#define MEMORY_FREE(pointer)                            vPortFree(pointer)
#endif



#endif /* INC_FANNYPACK_CONF_H_ */
